
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	// If the projectile is on screen... Do Stuff
	if (IsActive())
	{
		// Get the velocity for the direction that the projectile is being shot.
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		// Move the projectile that direction
		TranslatePosition(translation);

		Vector2 position = GetPosition(); // Current Position
		Vector2 size = s_pTexture->GetSize(); // Get the size of the projectile

		// Is the projectile off the screen deactivate it.
		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}
	// Move the Projectile Object
	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	// If the projectile is active draw it on the screen
	if (IsActive())
	{
		// Draw the projectile on the screen with the center color of white
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	// When the player shoots set the position of the projectile
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	// Activate the projectile object
	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}